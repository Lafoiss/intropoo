<?php
class User {

	protected $id;

	protected $nom;
	protected $mail;
	protected $annee;
	
	function __construct($id, $n=null, $m=null, $a=null){
		if ($id==null){
			$this->id    = null;
			$this->nom   = $n;
			$this->mail  = $m;
			$this->annee = $a;
		}
		else{
			$this->load($id);
		}
	}

	public function getId(){return $this->id;}
	public function getNom(){return $this->nom;}
	public function getMail(){return $this->mail;}
	public function getAnnee(){return $this->annee;}

	public function setNom($n) {$this->nom=$n;}	
	public function setMail($m){ $this->mail;}
	public function setAnnee($a){ $this ->annee;}

	public function save(){
		require ('connecte.php');
		$req = $bdd->prepare('INSERT INTO user(nom,mail,annee) values(?,?,?)');
		$req->bindParam(1,$this->nom);
		$req->bindParam(2,$this->mail);
		$req->bindParam(3,$this->annee);
		$req->execute();
	}


	protected function load($id){

		require ('connecte.php');
		$req = $bdd->prepare('SELECT * FROM user WHERE id=?');
		$req->bindParam(1,$id);
		$req->execute();
		$ligne=$req->fetch();
		$this->id = $ligne['id'];
		$this->nom   = $ligne['nom'];
		$this->mail  = $ligne['mail'];
		$this->annee = $ligne['annee'];

	}

	static function getList(){ //classe statique, méthode de classe

		$tabUser = array();
		require ('connecte.php');
		$req = $bdd->prepare('SELECT id FROM user');
		$req->execute();

		while ($ligne=$req->fetch()){
			
			$user = new User($ligne['id']);
			$tabUser[]= $user;
		}
		
		return $tabUser;

	}


}
